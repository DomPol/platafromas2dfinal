﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{

    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 10f;
    private bool jump;
    public float fuerzasalto = 10;
    public bool isGrounded = false;
    public GameObject graphics;
    private float scalaActual;
    [SerializeField] int fuerzaTrampolin = 10;
    //public Animator animator;
    
    void Awake () {
    rb2d = GetComponent<Rigidbody2D>();
    }


    void FixedUpdate () {
    rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);

    //animator.SetFloat("Velocidad", Math.Abs(rb2d.velocity.x));

    

    }
       
    void Update (){

    move = Input.GetAxis("Horizontal");
    jump = Input.GetButtonDown ("Jump");
    

    

    if (jump && isGrounded){
        
        jump=false;
        rb2d.AddForce (Vector2.up * fuerzasalto, ForceMode2D.Impulse);
        
    }

    
    

    scalaActual = graphics.transform.localScale.x;

    if (move > 0 && scalaActual < 0){
        graphics.transform.localScale = new Vector3(1,1,1);

    }
    else if (move < 0 && scalaActual > 0){
        graphics.transform.localScale = new Vector3(-1,1,1);
    }


      

}

     void OnTriggerEnter2D(Collider2D collision) 
    {
        isGrounded = true;
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        isGrounded = false;
    }

    void OnCollisionEnter2D(Collision2D collision) 
    {
        if (collision.gameObject.tag == "Trampolin"){
        rb2d.AddForce (Vector2.up * fuerzaTrampolin, ForceMode2D.Impulse);       
        }

    }

    
     

}
