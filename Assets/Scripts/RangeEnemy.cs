﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class RangeEnemy : MonoBehaviour
{
    [SerializeField] float rangeDistanceMin;
    [SerializeField] float rangeDistanceMax;
    float rangeDistance = 6;
    [SerializeField] Transform player;
    [SerializeField] float velocidadEnemigo;
 
    [SerializeField] Patrullero patrol;
    Color colorgizmo = Color.yellow;
 
    private void Awake()
    {
        rangeDistance = rangeDistanceMin;
        player = GameObject.FindGameObjectWithTag("Player").transform;
    }
 
    private void Update()
    {
        if (Mathf.Abs(Vector2.Distance(player.position, transform.position)) < rangeDistance)
        {
            colorgizmo = Color.red;
            rangeDistance = rangeDistanceMax;
            patrol.enabled = false;
            transform.position = Vector2.MoveTowards(transform.position, player.position, Time.deltaTime * velocidadEnemigo);
        }
        else
        {
            colorgizmo = Color.yellow;
            rangeDistance = rangeDistanceMin;
            patrol.enabled = true;
        }
    }
 
    private void OnDrawGizmos()
    {
        Gizmos.color = colorgizmo;
        Gizmos.DrawWireSphere(transform.position, rangeDistance);
    }
}
