﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class Patrullero : MonoBehaviour
{
    [SerializeField] Transform[] posiciones;
    [SerializeField] int posactual = 0;
    [SerializeField] float velocidadEnemigo = 1.0f;

 
    private void Update()
    {
        
        if(Mathf.Abs(Vector2.Distance(posiciones[posactual].position, transform.position)) < 0.1f)
        {
            posactual++;
            if (posactual >= posiciones.Length)
            {
                posactual = 0;
            }
        }
 
        transform.position = Vector2.MoveTowards(transform.position, posiciones[posactual].position, Time.deltaTime*velocidadEnemigo);
    }

}