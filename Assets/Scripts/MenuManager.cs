﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    [SerializeField] AudioSource audioSource;

    public void PulsaPlay()
    {
        Debug.LogError("He pulsado Play");


        //  gameObject.GetComponent<AudioSource>().Play();


        SceneManager.LoadScene("Gameplay");
    }
    public void PulsaOptions()
    {
        Debug.LogError("He pulsado Options");

        SceneManager.LoadScene("Options");
    }
    public void PulsaCredits()
    {
        Debug.LogError("He pulsado Credits");

        SceneManager.LoadScene("Credits");
    }
}
